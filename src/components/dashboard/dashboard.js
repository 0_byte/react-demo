import React from "react";
import {connect} from "react-redux";
import { Link } from 'react-router-dom'

class Dashboard extends React.Component {

    render() {
        const {signupReducer } = this.props;
        console.debug(signupReducer.user);

        return (
            <div>
                <div className="signup-header">
                    <div className='label-signup color-main-blue'>Dashboard</div>
                </div>

                <Link to='/'>Sign-up again</Link>
            </div>
        );

    }
}

function mapStateToProps(state) {
    return {
        signupReducer: state.signupReducer
    }
}


export default connect(mapStateToProps)(Dashboard);