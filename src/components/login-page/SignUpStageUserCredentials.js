import React, {Component} from "react";
import {ControlLabel, FormControl, FormGroup} from "react-bootstrap";
import MdArrowForward from 'react-icons/lib/md/arrow-forward'
import {REGEX_EMAIL_VALIDATION} from "../../constant/constatnts";
import SignUpHeader from "./SignupHeader";

export default class SignUpStageUserCredentials extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            email: '',
            password: '',
            passwordConfirm: '',

            emailValidator: {status: null, message: 'EMAIL'},
            passwordValidator: {status: null, message: 'PASSWORD'},
            passwordConfirmValidator: {status: null, message: 'CONFIRM PASSWORD'},

            isStrictValidation: false,
            isValid: false

        };

        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handlePasswordConfirmChange = this.handlePasswordConfirmChange.bind(this);
        this.nextScreenHandler = this.nextScreenHandler.bind(this);
        this.finalValidation = this.finalValidation.bind(this);
    }

    getEmailValidationState() {
        // TODO should check if username is available

        const length = this.state.email.length;
        let response = this.state.emailValidator;

        if (length === 1) {
            response.status = 'error';
            response.message = 'Email is required';
        } else if (length > 1) {
            if (REGEX_EMAIL_VALIDATION.test(this.state.email)) {
                response.status = 'success';
                response.message = 'EMAIL'
            } else {
                response.message = 'Email is not valid';
                response.status = 'error';
            }
        }

        if (response.status === null && this.state.isStrictValidation) {
            response = {
                status: 'error',
                message: response.message + ' IS REQUIRED'
            }
        }

        this.state.emailValidator = response
    }

    getPassWordValidator() {
        const length = this.state.password.length;
        let response = this.state.passwordValidator;

        if ((length >= 1 && length < 5)) {
            response = {
                status: 'error',
                message: 'PASSWORD SHOULD BE 6 CHARACTERS LONG'
            }
        } else if (length > 5) {
            response = {
                status: 'success',
                message: 'PASSWORD'
            }
        }

        if (response.status === null && this.state.isStrictValidation) {
            response = {
                status: 'error',
                message: response.message + ' IS REQUIRED'
            }
        }

        this.state.passwordValidator = response
    }

    getPassWordConfirmValidator() {
        let response = this.state.passwordConfirmValidator;

        if (this.state.password.length > 0
            && this.state.passwordConfirm.length > 0) {
            if (this.state.password !== this.state.passwordConfirm) {
                response = {
                    status: 'error',
                    message: 'PASSWORD DOES NOT MATCH'
                };
            } else {
                response = {
                    status: 'success',
                    message: 'CONFIRM PASSWORD'
                };
            }
        }

        if (response.status === null && this.state.isStrictValidation) {
            response = {
                status: 'error',
                message: response.message + ' IS REQUIRED'
            }
        }

        this.state.passwordConfirmValidator = response
    }

    handleEmailChange(e) {
        this.setState({email: e.target.value});
    }

    handlePasswordChange(e) {
        this.setState({password: e.target.value});
    }

    handlePasswordConfirmChange(e) {
        this.setState({passwordConfirm: e.target.value});
    }

    nextScreenHandler() {
        this.finalValidation();
        if (this.state.isValid) {
            const {dispatch, SignUpActions} = this.props.actions;
            dispatch(SignUpActions.signupUserCredentials({
                email: this.state.email,
                password: this.state.password
            }));
        }
    }

    finalValidation() {
        this.getEmailValidationState();
        this.getPassWordValidator();
        this.getPassWordConfirmValidator();

        if (this.state.emailValidator.status === 'success'
            && this.state.passwordValidator.status === 'success'
            && this.state.passwordConfirmValidator.status === 'success'
        ) {
            this.state.isStrictValidation = false;
            this.state.isValid = true;
        } else {
            this.setState({
                isStrictValidation: true,
                isValid: false
            })
        }
    }

    render() {
        this.getEmailValidationState();
        this.getPassWordValidator();
        this.getPassWordConfirmValidator();

        this.state.isStrictValidation = false;

        return (
            <div className="signup-wrapper">
                <SignUpHeader progress={this.props.progress} title='Signup'/>

                <form className="signup-container signup-form user-credentials-wrapper">
                    <FormGroup
                        controlId="userName"
                        validationState={this.state.emailValidator.status}>
                        <ControlLabel>{this.state.emailValidator.message}</ControlLabel>
                        <FormControl
                            type="email"
                            bsSize="large"
                            value={this.state.email}
                            placeholder=""
                            onChange={this.handleEmailChange}
                            className="input-main"
                        />
                    </FormGroup>

                    <FormGroup
                        controlId="userPassword"
                        validationState={this.state.passwordValidator.status}>
                        <ControlLabel>{this.state.passwordValidator.message}</ControlLabel>
                        <FormControl
                            type="password"
                            bsSize="large"
                            value={this.state.password}
                            placeholder=""
                            onChange={this.handlePasswordChange}
                            className="input-main"
                        />
                    </FormGroup>

                    <FormGroup
                        controlId="userPasswordConfirm"
                        validationState={this.state.passwordConfirmValidator.status}>
                        <ControlLabel>{this.state.passwordConfirmValidator.message}</ControlLabel>
                        <FormControl
                            type="password"
                            bsSize="large"
                            value={this.state.passwordConfirm}
                            placeholder=""
                            onChange={this.handlePasswordConfirmChange}
                            className="input-main"
                        />
                    </FormGroup>
                </form>
                <div className="signup-footer">
                    <div className="signup-footer-container">
                         <span className="button-next pull-right" onClick={this.nextScreenHandler}>
                                Next <MdArrowForward/>
                        </span>
                    </div>
                </div>

            </div>
        )
    }
}
