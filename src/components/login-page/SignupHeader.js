import React from "react";
import {ProgressBar} from "react-bootstrap";

class SignUpHeader extends React.Component {

    render() {
        return (
            <div className="signup-header">
                <div className='label-signup color-main-blue'>{this.props.title}</div>
                <ProgressBar now={this.props.progress} className="no-border-radius"/>
            </div>
        )
    }
}

export default SignUpHeader