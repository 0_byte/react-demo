import React from "react";
import * as CONSTANTS from './../../constant/constatnts'
import {connect} from "react-redux";
import SignUpStageUserCredentials from "./SignUpStageUserCredentials";
import SignUpStageUserInfo from "./SignUpStageUserInfo";
import SignUpStageFinish from "./SignUpStageFinish";
import * as SignUpActions from './../../actions/signup-actions';

class SignUpPage extends React.Component {

    constructor(){
        super();
        this.state = {
            currentStage: CONSTANTS.SignUpStateUserCreation
        };
    }

    componentDidMount(){
        const {dispatch } = this.props;
        dispatch(SignUpActions.signupReset());
    }

    componentWillReceiveProps(nextProp){
        if(nextProp.signupReducer.currentStage){
            this.setState({
                currentStage: nextProp.signupReducer.currentStage
            });
        }
    }

    parseStage(stageName, actions) {
        switch (stageName){
            case CONSTANTS.SignUpStateUserCreation:
                return <SignUpStageUserCredentials progress={30} actions={actions}/>;
            case CONSTANTS.SignUpStateUserInfo:
                return <SignUpStageUserInfo progress={60} actions={actions}/>;
            case CONSTANTS.SignUpStateSuccess:
                return <SignUpStageFinish progress={100}/>;
        }
    }

    render() {
        const {dispatch } = this.props;
        const actions = ({SignUpActions, dispatch});

        return (
            this.parseStage(this.state.currentStage, actions)
        );

    }
}

function mapStateToProps(state) {
    return {
        signupReducer: state.signupReducer
    }
}


export default connect(mapStateToProps)(SignUpPage);