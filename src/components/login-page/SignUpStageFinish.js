import React, {Component} from "react";
import SignUpHeader from "./SignupHeader";
import {MdDone} from "react-icons/lib/md/index";
import {MdArrowForward} from "react-icons/lib/md/index";
import {Button} from "react-bootstrap";
import { Link } from 'react-router-dom'

class SignUpStageFinish extends React.Component {

    render(){
        return(
            <div className="signup-wrapper">
                <SignUpHeader progress={this.props.progress} title='Thank you!'/>
                <div className="signup-container signup-form user-info-success-wrapper">
                    <div className="circle-box">
                        <MdDone size={200}/>
                    </div>
                    <div>
                        <Button bsSize="large" onClick={this.handleGenderChange} className="button-next button-success">
                            <Link to='/dashboard'  style={{display: 'block', height: '100%'}}> Go to Dashboard <MdArrowForward style={{verticalAlign: 'text-top'}}/></Link>
                        </Button>

                    </div>
                </div>
            </div>
        )
    }
}

export default SignUpStageFinish;