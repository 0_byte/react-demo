import React, {Component} from "react";
import SignUpHeader from "./SignupHeader";
import {MdArrowForward} from "react-icons/lib/md/index";
import {Button, ButtonGroup, DropdownButton, Form, FormControl, FormGroup, MenuItem} from "react-bootstrap";
import moment from 'moment'
import {SignUpStateUserCreation} from "../../constant/constatnts";

class SignUpStageUserInfo extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            sourceOptions: [
                {id: 1, name: 'Inernet'},
                {id: 2, name: 'From the street'},
                {id: 3, name: 'From friend'}
            ],

            genderOptions: [
               'MALE',
               'FEMALE',
               'UNSPECIFIED'
            ],

            selectedSourceOption: null,
            selectedGender: false,
            selectedDay: false,
            selectedMonth: false,
            selectedYear: false,

            hasErrors: false,
            validateMessage: ''
        };

        this.sourceHandler = this.sourceHandler.bind(this);
        this.handleGenderChange = this.handleGenderChange.bind(this);
        this.handleDayChange = this.handleDayChange.bind(this);
        this.handleMonthChange = this.handleMonthChange.bind(this);
        this.handleYearChange = this.handleYearChange.bind(this);
        this.validateForm = this.validateForm.bind(this);
        this.validateYear = this.validateYear.bind(this);
        this.nextScreenHandler = this.nextScreenHandler.bind(this);
        this.prevScreenHandler = this.prevScreenHandler.bind(this);
    }

    nextScreenHandler(){
        let validateForm = this.validateForm();
        if(!validateForm.status){
            this.setState({
                hasErrors: true,
                validateMessage: validateForm.message
            });
        }else{
            const {dispatch, SignUpActions} = this.props.actions;
            let timeStampDate = ( moment(this.state.selectedDay + '/' + this.state.selectedMonth + '/' + this.state.selectedYear,
                            'DD/MM/YYYY')
                    .unix()
                ),
                sourceOption = this.state.selectedSourceOption !== null ? this.state.selectedSourceOption[0].name : '';
            dispatch(SignUpActions.signupUserInfoAction({
                date_of_birth: timeStampDate,
                gender: this.state.selectedGender,
                sourceOption: sourceOption
            }));
        }
    }

    prevScreenHandler(){
        const {dispatch, SignUpActions} = this.props.actions;
        dispatch(SignUpActions.signupStateChanged(SignUpStateUserCreation));
    }

    handleDayChange(e){
        let day = e.target.value;
        if(day < 1 || day > 31){
            day = day.slice(0, -1);
        }
        this.setState({
            selectedDay: day
        })
    }

    handleMonthChange(e){
        let month = e.target.value;
        if(month < 1 || month > 12){
            month = month.slice(0, -1);
        }
        this.setState({
            selectedMonth: month
        })
    }

    handleYearChange(e){
        let year = e.target.value;

        if(year.length > 3){
            if(!this.validateYear(year)){
                year = year.slice(0, -1);
            }
        }

        this.setState({
            selectedYear: year
        })
    }

    validateYear(year){
        return year > 1850 && year <= (new Date()).getFullYear()
    }

    handleGenderChange(e){
        this.setState({
            selectedGender: e.target.value
        })
    }

    sourceHandler(e){
        this.setState({
            selectedSourceOption: this.state.sourceOptions.filter((el) => {
                return el.id === e ;
            })
        })
    }

    validateForm(){
        let isValidState = {
            status: false,
            message: ''
        };

        if(this.state.selectedDay === false
            || this.state.selectedMonth === false
            || this.state.selectedYear === false){
            isValidState.message = 'Please enter your date of birth';
            return isValidState;
        }

        if(!this.validateYear(this.state.selectedYear)){
            isValidState.message = 'Specify your birth year';
            return isValidState;
        }

        let date = this.state.selectedDay + '/' + this.state.selectedMonth + '/' + this.state.selectedYear;
            if(!moment(date , "DD/MM/YYYY").isValid()){
                isValidState.message = 'Selected date is not exist';
                return isValidState;
            }

        if(Math.floor(moment(new Date()).diff(moment(date,"DD/MM/YYYY"),'years',true)) < 18){
            isValidState.message = 'Your age is not eligible for this content';
            return isValidState
        }

        if(this.state.selectedGender === false){
            isValidState.message = 'Please select your gender';
            return isValidState
        }

        isValidState.status = true;
        return isValidState;
    }

    render(){
        let dropdownSourceTitle = this.state.selectedSourceOption !== null
            ? this.state.selectedSourceOption[0].name
            : '...',
            errorsDisplayStyle = this.state.hasErrors ? {display: 'block'} : {},
            selectedDropdown = this.state.selectedSourceOption !== null
                ? 'dropdown-toggle-active'
                : '';

        return(
            <div className="signup-wrapper">
                <SignUpHeader progress={this.props.progress} title='Signup'/>
                <div className="signup-container signup-form user-info-wrapper">
                    <div className="user-info-box">
                        <div className="box-label">
                            DATE OF BIRTH
                        </div>
                        <Form inline>
                            <FormGroup>
                                <FormControl bsSize="large" type="number" placeholder="DD" className="firstChild"
                                             value={this.state.selectedDay} onChange={this.handleDayChange}/>
                            </FormGroup>
                            <FormGroup>
                                <FormControl bsSize="large" type="number" placeholder="MM"
                                             value={this.state.selectedMonth} onChange={this.handleMonthChange}/>
                            </FormGroup>
                            <FormGroup>
                                <FormControl bsSize="large" type="number" placeholder="YYYY" className="lastChild"
                                             value={this.state.selectedYear} onChange={this.handleYearChange}/>
                            </FormGroup>
                        </Form>
                    </div>

                    <div className="user-info-box">
                        <div className="box-label">
                            GENDER
                        </div>
                        <ButtonGroup>
                            {this.state.genderOptions.map((el, indx)=>{
                                return <Button bsSize="large" key={indx} onClick={this.handleGenderChange}
                               value = {el} className={el === this.state.selectedGender ? 'selected' : ''}>
                                    {el}</Button>;
                            })}
                        </ButtonGroup>
                    </div>

                    <div className="user-info-box">
                        <div className="box-label">
                            WHERE DID YOU HERE ABOUT US?
                        </div>
                        <div className="user-info-source">
                            <DropdownButton id="dropdown-source" title={dropdownSourceTitle} bsSize="large" onSelect={this.sourceHandler}
                             className={selectedDropdown}>
                                {this.state.sourceOptions.map((el)=>{
                                    return <MenuItem key={el.id} eventKey={el.id}>{el.name}</MenuItem>;
                                })}

                            </DropdownButton>
                        </div>
                    </div>

                    <div className="user-info-box error-block" style={errorsDisplayStyle}>
                        {this.state.validateMessage}
                    </div>
                </div>
                <div className="signup-footer">
                    <div className="signup-footer-container">
                         <span className="button-previous pull-left" onClick={this.prevScreenHandler}>
                                Back
                        </span>
                         <span className="button-next pull-right" onClick={this.nextScreenHandler}>
                                Next <MdArrowForward/>
                        </span>
                    </div>
                </div>
            </div>
            )
    }
}

export default SignUpStageUserInfo;