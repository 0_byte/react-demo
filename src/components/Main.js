import React from 'react'
import { Switch, Route } from 'react-router-dom'
import SignUpPage from "./login-page/SignUpPage";
import Dashboard from './dashboard/dashboard';


const Main = () => (
    <main>
        <Switch>
            <Route exact path='/' component={SignUpPage}/>
            <Route exact path='/dashboard' url="/dashboard" component={Dashboard}/>
        </Switch>
    </main>
)

export default Main
