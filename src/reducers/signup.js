import * as actions from "./../actions/index";
import * as constatns from "./../constant/constatnts";
import {merge} from 'lodash';

const signupReducer = (state = {
    currentStage: constatns.SignUpStateUserCreation,
    user: {
        email: '',
        password: '',
        date_of_birth: '',
        gender: '',
        sourceOption: ''
    }
}, action) => {
    switch (action.type) {
        case actions.SIGNUP_STATE_CHANGED:
            return merge({}, state, {
                currentStage: action.newState
            });
        case actions.SIGNUP_USER_CREDENTIALS_ACTION:
            return merge({}, state, {
                user: {
                    email: action.userInfo.email,
                    password: action.userInfo.password
                },
                currentStage: action.newState
            });
        case actions.SIGNUP_USER_SET_USER_INFO_ACTION:
            return {
                user: {
                    email: state.user.email,
                    password: state.user.password,
                    date_of_birth: action.userInfo.date_of_birth,
                    gender: action.userInfo.gender,
                    sourceOption: action.userInfo.sourceOption
                },
                currentStage: action.newState
            };
        case actions.SIGNUP_RESET:
        default:
            return state;
    }
};

export default signupReducer;