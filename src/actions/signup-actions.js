import * as actions from "./index";
import {SignUpStateUserInfo, SignUpStateSuccess} from "../constant/constatnts";

export const signupStateChanged = (newState) => {
  return{
      type: actions.SIGNUP_STATE_CHANGED,
      newState: newState
  }
};

export const signupUserCredentials = (userInfo) => {
    return{
        type: actions.SIGNUP_USER_CREDENTIALS_ACTION,
        userInfo: userInfo,
        newState: SignUpStateUserInfo
    }
};

export const signupUserInfoAction = (userInfo) => {
    return{
        type: actions.SIGNUP_USER_SET_USER_INFO_ACTION,
        userInfo: userInfo,
        newState: SignUpStateSuccess
    }
};

export const signupReset = () => {
    return{
        type: actions.SIGNUP_RESET
    }
};