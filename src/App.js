import React from 'react';
import ReactDOM from 'react-dom';
import './styles/main.css';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import loginApp from './reducers';
import { BrowserRouter } from 'react-router-dom'


import App from "./components/App";

let store = createStore(
    loginApp,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDOM.render(
    <BrowserRouter>
        <Provider store={store}>
            <App/>
        </Provider>
    </BrowserRouter>, document.getElementById('app'));