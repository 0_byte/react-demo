
export const SignUpStateUserCreation = 'user';
export const SignUpStateUserInfo = 'user-info';
export const SignUpStateSuccess = 'success';

export const REGEX_EMAIL_VALIDATION = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const SignUpStagesNames = [
    SignUpStateUserCreation,
    SignUpStateUserInfo,
    SignUpStateSuccess
];
